#!/bin/sh

work_dir=$(dirname $0)

echo 'GF_SECURITY_ADMIN_PASSWORD='$(pass perso/Servers/Grafana/admin) >$work_dir/secrets.env
echo 'GF_SMTP_PASSWORD='$(pass perso/Email/Gandi/system@lordran.net) >>$work_dir/secrets.env
echo 'GF_DATABASE_PASSWORD='$(pass perso/Servers/Grafana/pgsql) >>$work_dir/secrets.env
